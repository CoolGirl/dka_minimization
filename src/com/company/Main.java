package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class Main {

    public static final int DEVIL_NODE_NUMBER = 0;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("minimization.in"));
        DeterminedAutomation firstAutomation = createDeterminedAutomationFromInput(br);
        // DeterminedAutomation secondAutomation = createDeterminedAutomationFromInput(br);
        PrintWriter pw = new PrintWriter("minimization.out");
        //pw.println(IsomorphicTest.areIsomorphic(firstAutomation, secondAutomation) ? "YES" : "NO");
        pw.close();
    }

    static class Minimization {
        static Queue<NodePair> queue;
        static HashMap<Integer, HashMap<Character, Integer>> reversedEdges;
        static DeterminedAutomation automation;
        static boolean marked[][];
        static int[] snm;
        static int maxSetNumber = 1;

        public Minimization(DeterminedAutomation auto) {
            automation = auto;
            marked = new boolean[automation.nodes.size()][automation.nodes.size()];
            snm = new int[automation.nodes.size() + 1];
            for (int i = 0; i < automation.nodes.size(); i++) {
                for (Map.Entry<Character, Integer> pair : automation.nodes.get(i).transitions.entrySet()) {
                    reversedEdges.get(pair.getValue()).put(pair.getKey(), i);
                }
            }
        }

        public static DeterminedAutomation minimize() {
            //initial partition on the two sets: accessing and non-accessing
            for (int j = 0; j < snm.length; j++) {
                for (int i = 0; i <= j; i++) {
                    if (automation.accessingNodes.contains(j)) {
                        snm[j] = maxSetNumber;
                        if (!automation.accessingNodes.contains(i)) {
                            marked[i][j] = true;
                            marked[j][i] = true;
                        }
                    } else {
                        snm[j] = 0;
                        if (automation.accessingNodes.contains(i)) {
                            marked[i][j] = true;
                            marked[j][i] = true;
                        }
                    }
                    if (!marked[i][j]) {
                        queue.add(new NodePair(i, j));
                    }
                }
            }
            while (!queue.isEmpty()) {
                NodePair currentPair = queue.poll();
                if (reversedEdges.containsKey(currentPair.u) && reversedEdges.containsKey(currentPair.v)) {
                    for (char letter = 'a'; letter <= 'z'; letter++) {
                        if (reversedEdges.get(currentPair.u).containsKey(letter) &&
                                reversedEdges.get(currentPair.v).containsKey(letter)) {
                            if (snm[reversedEdges.get(currentPair.u).get(letter)] !=
                                    snm[reversedEdges.get(currentPair.v).get(letter)]) {
                                divideClass();
                            }
                        }
                    }
                }
            }

        }

        static class NodePair {
            int u;
            int v;

            public NodePair(int u, int v) {
                this.u = u;
                this.v = v;
            }
        }
       /* static class ReversedEdge{
            int from;
            int to;
            char symbol;

            public ReversedEdge(int from, int to, char symbol){
                this.from = from;
                this.to = to;
                this.symbol = symbol;
            }
        }  */
    }

    static DeterminedAutomation createDeterminedAutomationFromInput(BufferedReader br) throws IOException {
        String[] s = br.readLine().split(" ");
        int accessingNodesNumber = Integer.parseInt(s[2]);
        int nodesNumber = Integer.parseInt(s[0]);
        int transitionsNumber = Integer.parseInt(s[1]);
        DeterminedAutomation automation = new DeterminedAutomation(nodesNumber + 1, accessingNodesNumber);
        s = br.readLine().split(" ");
        for (int i = 0; i < accessingNodesNumber; i++) {
            automation.accessingNodes.add(Integer.parseInt(s[i]));
        }
        for (int i = 0; i < transitionsNumber; i++) {
            s = br.readLine().split(" ");
            automation.nodes.get(Integer.parseInt(s[0])).transitions.put(s[2].charAt(0), Integer.parseInt(s[1]));
        }
        for (int i = 0; i < automation.nodes.size(); i++) {
            for (char letter = 'a'; letter <= 'z'; letter++) {
                if (!automation.nodes.get(i).transitions.containsKey(letter)) {
                    automation.nodes.get(i).transitions.put(letter, DEVIL_NODE_NUMBER);
                }
            }
        }
        return automation;
    }

    static class DeterminedAutomation {
        ArrayList<Node> nodes;
        HashSet<Integer> accessingNodes;

        public DeterminedAutomation(int nodesNumber, int accessingNodesNumber) {
            nodes = new ArrayList<Node>();
            for (int i = 0; i <= nodesNumber; i++) {
                nodes.add(new Node());
            }
            accessingNodes = new HashSet<Integer>(accessingNodesNumber);
        }

        static class Node {
            HashMap<Character, Integer> transitions;

            public Node() {
                transitions = new HashMap<Character, Integer>();
            }
        }
    }
}